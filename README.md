# Weather App

## Vercel Hosting link : https://weather-app-lilac-pi.vercel.app/

## Built on : React

## Instruction 

* Insert location and click search button.

* Weather details are displayed on every valid location entries.

* In case of Invalid location entry "Location not Found!!" will displayed.



## API-Name : open weather API 

### API :  `https://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=bc0b957686287ca96a31f70b7d9dff04`



