import React, { Component } from "react";

import MainContainer from "./component/MainContainer/MainContainer";
import "./App.css"
class App extends Component {
  render() {
    return (
      <div>
        <h1 style={{ fontFamily: "sans-serif", color: "darkblue",marginLeft: "8rem" }}>
          WEATHER APP
        </h1>
         <MainContainer />
      </div>
    );
  }
}

export default App;
