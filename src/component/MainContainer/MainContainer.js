import React, { Component } from "react";
import FetchData from "../services/FetchData";
import CityWeatherCard from "../CityWeatherCard/CityWeatherCard";
import "./MainContainer.css";
class MainContainer extends Component {
  state = {
    City: "",
    weatherData: null,
  };

  handleChange(event) {
    this.setState({ City: event.target.value });
  }

  handleInput(event) {
    event.preventDefault();

    event.currentTarget.firstChild.value = "";

    FetchData(this.state.City).then((Response) => {
      this.setState({ weatherData: Response });
    });
  }

  render() {
    return (
      <div className="mainContainer">
        <div className="searchBar">
          <form className="inputTab" onSubmit={this.handleInput.bind(this)}>
            <input
              id="sd"
              type="text"
              value={this.City}
              placeholder="Enter location"
              onChange={this.handleChange.bind(this)}
            ></input>
            <button type="submit" className="submitButton">
              Search
            </button>
          </form>
        </div>

        {this.state.weatherData === null ? (
          <h1>Please Insert Location</h1>
        ) : this.state.weatherData.name === undefined ? (
          <h1>Location Not Found!!</h1>
        ) : (
          <CityWeatherCard data={this.state} />
        )}
      </div>
    );
  }
}
export default MainContainer;
