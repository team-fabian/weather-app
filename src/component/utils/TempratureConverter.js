function TempratureConverter(kelvin) {
    kelvin = parseFloat(kelvin);
    
  if (typeof kelvin !== "number" || isNaN(kelvin)) {
    throw new Error("Input must be a valid number");
  }
 

 
  const celsius = kelvin - 273.15;
  return celsius.toFixed(1);
}

export default TempratureConverter ;