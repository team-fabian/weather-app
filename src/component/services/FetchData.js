function FetchData(cityName = "Bangalore") {
  return fetch(
    `https://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=bc0b957686287ca96a31f70b7d9dff04`
  )
    .then((Response) => Response.json())

    .catch((error) => {
      console.log(error.message);
      throw new Error("Location not found !!!");
    });
}

export default FetchData;

