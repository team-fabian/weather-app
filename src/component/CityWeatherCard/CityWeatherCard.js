import React, { Component } from "react";
import "./CityWeatherCard.css";
import TempratureConverter from "../utils/TempratureConverter";
export default class CityWeatherCard extends Component {
  render() {
    const { name } = this.props.data.weatherData;
    const { humidity, pressure, temp, temp_max, temp_min } =
      this.props.data.weatherData.main;

    const normalTemp = TempratureConverter(temp);
    const maxTemp = TempratureConverter(temp_max);
    const minTemp = TempratureConverter(temp_min);
    return (
      <div className="weatherCard">
        <h1>Today's Weather</h1>
        <div className="CityTitle">
          <h1 className="CityName">{name},</h1>
          <h3>{normalTemp} celsius </h3>
        </div>

        <div className="windy"></div>
        <div className="information">
          <h1>Weather Information</h1>

          <div className="row">
            <div className="card">
              <i class="fa-solid fa-temperature-arrow-up fa-2x"></i>
              <h4>Max Temp : {maxTemp}</h4>
            </div>
            <div className="card">
              <i class="fa-solid fa-temperature-arrow-down fa-2x"></i>
              <h4>Min Temp : {minTemp}</h4>
            </div>
          </div>
          <div className="row">
            <div className="card">
              <i class="fa-solid fa-wind fa-2x"></i>
              <h4>Pressure : {pressure}</h4>
            </div>
            <div className="card">
              <i class="fa-sharp fa-solid fa-circle"></i>
              <h4>Humidity : {humidity}</h4>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
